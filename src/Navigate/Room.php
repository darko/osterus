<?php

namespace Osterus\Navigate;

class Room
{
    public int $x;
    public int $y;

    public function __construct(int $x, int $y)
    {
        $this->x = $x;
        $this->y = $y;
    }

    public function __toString()
    {
        return $this->x . ' ' . $this->y;
    }
}
