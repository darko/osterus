<?php

namespace Osterus\Navigate;

interface Navigator
{
    public function navigate(Position $position);
}
