<?php

namespace Osterus\Navigate;

use Osterus\Exceptions\IncorrectPositionException;

class Position
{
    public int $x;
    public int $y;
    public string $heading;
    private Room $room;

    public function __construct(Room $room, int $x, int $y, string $heading)
    {
        $this->x = $x;
        $this->y = $y;
        $this->heading = $heading;
        $this->room = $room;
    }
  

    public function __toString()
    {
        return $this->x . ' ' . $this->y . ' ' . $this->heading;
    }

    public function validate()
    {
        if (
            $this->x < 0
            or $this->x > $this->room->x
            or $this->y < 0
            or $this->y > $this->room->y
        ) {
            throw new IncorrectPositionException(
                sprintf(
                    'Incorrect Position: The robot hit the wall of the room at <%d, %d, %s>',
                    $this->x,
                    $this->y,
                    $this->heading
                )
            );
        }
    }
}
