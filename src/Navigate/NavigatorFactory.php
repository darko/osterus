<?php

namespace Osterus\Navigate;

use Osterus\Exceptions\IncorrectCommandException;

class NavigatorFactory
{
    public static function make(string $command, Position $position)
    {
        $className = "Osterus\\Navigate\\Actions\\Navigate" . ($command == 'F' ? $position->heading : $command);

        if (! class_exists($className)) {
            throw new IncorrectCommandException(sprintf('Incorrect navigation command "%s"', $command));
        }
    
        return new $className;
    }
}
