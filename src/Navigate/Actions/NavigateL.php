<?php

namespace Osterus\Navigate\Actions;

use Osterus\Navigate\Navigator;
use Osterus\Navigate\Position;

class NavigateL implements Navigator
{
    private $leftTurnMapping = [
        'N' => 'W',
        'W' => 'S',
        'S' => 'E',
        'E' => 'N',
    ];

    public function navigate(Position $position)
    {
        $position->heading = $this->leftTurnMapping[$position->heading];
    }
}
