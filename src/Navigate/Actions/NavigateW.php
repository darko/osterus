<?php

namespace Osterus\Navigate\Actions;

use Osterus\Navigate\Navigator;
use Osterus\Navigate\Position;

class NavigateW implements Navigator
{   
    public function navigate(Position $position)
    {
        $position->x--;
    }
}