<?php

namespace Osterus\Navigate\Actions;

use Osterus\Navigate\Navigator;
use Osterus\Navigate\Position;

class NavigateR implements Navigator
{
    private $rightTurnMapping = [
        'N' => 'E',
        'E' => 'S',
        'S' => 'W',
        'W' => 'N',
    ];
    
    public function navigate(Position $position)
    {
        $position->heading = $this->rightTurnMapping[$position->heading];
    }
}