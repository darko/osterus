<?php

if (! function_exists('strToArray')) {
    function strToArray($str)
    {
        return explode(' ', $str);
    }
}

if (! function_exists('parseInput')) {
    function parseInput($input)
    {
        $fp = fopen("php://memory", 'r+');
        fputs($fp, $input);
        rewind($fp);

        $input = [];

        $c = 0;
        $n = 0;
        while ($line = trim(fgets($fp))) {
            if ($c == 0) {
                $input['room'] = $line;
            } else {
                if ($c % 2 == 0) {
                    $input['robots'][$n]['commands'] = $line;
                    $n++;
                } else {
                    $input['robots'][$n]['position'] = $line;
                }
            }
            $c++;
        }
        fclose($fp);

        return $input;
    }
}
