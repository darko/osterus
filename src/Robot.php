<?php

namespace Osterus;

use Osterus\Navigate\NavigatorFactory;
use Osterus\Navigate\Position;
use Osterus\Navigate\Room;

class Robot
{
    private Position $position;
    private $commands = [];

    public function setPosition(Room $room, string $position)
    {
        $this->position = new Position($room, ...strToArray($position));

        return $this;
    }

    public function setCommands(string $commands)
    {
        $this->commands = str_split($commands);

        return $this;
    }

    public function run()
    {
        foreach ($this->commands as $command) {   
                  
            NavigatorFactory::make($command, $this->position)->navigate($this->position);       

            $this->position->validate();
        }

        return $this->position;
    }
}
