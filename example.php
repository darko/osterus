<?php

use Osterus\Navigate\Room;
use Osterus\Robot;

require __DIR__ . '/vendor/autoload.php';

$input = <<<'EOD'
5 5
1 2 N
LFLFLFLFF
3 3 E
FFRFFRFRRF 
EOD;


$input = parseInput($input);
$output = '';

$room = new Room(...strToArray($input['room']));

foreach ($input['robots'] as $robot) {
    try {
        $output .= (new Robot())->setPosition($room, $robot['position'])->setCommands($robot['commands'])->run() . PHP_EOL;
    } catch (\Throwable $th) {
        $output .= $th->getMessage() . PHP_EOL;
    }
}

echo $output;
// 1 3 N
// 5 1 E
