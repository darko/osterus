<?php

use Osterus\Exceptions\IncorrectCommandException;
use Osterus\Exceptions\IncorrectPositionException;
use Osterus\Navigate\Room;
use Osterus\Robot;
use PHPUnit\Framework\TestCase;

class RobotTest extends TestCase
{

    /**
     * @test
     * @dataProvider navigationData
     */
    public function is_on_correct_final_position($position, $commands, $expected)
    {

        $room = new Room(5,5);

        $result = (new Robot())->setPosition($room, $position)->setCommands($commands)->run();

        $this->assertEquals($expected, $result);
    }

    public function navigationData()
    {
        return [
            [    
              '1 2 N',
              'LFLFLFLFF',
              '1 3 N'                        
            ],
            [    
              '3 3 E',
              'FFRFFRFRRF',
              '5 1 E'                        
            ],
        ];
    }

    /** @test */
    public function incorrect_command_throws_exception()
    {
        $room = new Room(5,5);

        $this->expectException(IncorrectCommandException::class);

        $result = (new Robot())->setPosition($room, '1 2 N')->setCommands('LFLBLF')->run();

    }

    /** 
     * @test 
     * @dataProvider badCommands
    */
    public function position_outside_of_the_room_throws_exception($command)
    {
        $room = new Room(2,2);        

        $this->expectException(IncorrectPositionException::class);

        $result = (new Robot())->setPosition($room, '1 1 N')->setCommands($command)->run();

    }

    public function badCommands()
    {
        return [
            ['FF'],
            ['RFF'],
            ['LFF'],
            ['LLFF'],
        ];
    }


    

}