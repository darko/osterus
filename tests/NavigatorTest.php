<?php

use Osterus\Navigate\NavigatorFactory;
use Osterus\Navigate\Position;
use Osterus\Navigate\Room;
use PHPUnit\Framework\TestCase;

class NavigatorTest extends TestCase
{



  /**
   * @test
   * @dataProvider navigationData
   */
  public function is_on_correct_position_after_navigate_action($position, $command, $expected)
  {
    $room = new Room(2, 2);
    
    $position = new Position($room, ...strToArray($position));

    NavigatorFactory::make($command, $position)->navigate($position);

    $this->assertEquals($expected, (string) $position);
  }


  public function navigationData()
  {
      return [
          [ // turn left   
            '1 1 N',
            'L',
            '1 1 W'                        
          ],
          [ // turn right   
            '1 1 N',
            'R',
            '1 1 E'                        
          ],
          [ // move north 
            '1 1 N',
            'F',
            '1 2 N'                       
          ],
          [ // move east 
            '1 1 E',
            'F',
            '2 1 E'                       
          ],
          [ // move west 
            '1 1 W',
            'F',
            '0 1 W'                       
          ],
          [ // move south 
            '1 1 S',
            'F',
            '1 0 S'                       
          ],
         
      ];
  }
    

}